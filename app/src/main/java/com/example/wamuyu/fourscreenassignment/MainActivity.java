package com.example.wamuyu.fourscreenassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button firstScreen = (Button) findViewById(R.id.button);
        firstScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, FirstActivity.class);
                startActivity(i);
            }
        });

        Button secondScreen = (Button) findViewById(R.id.button2);
        secondScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, SecondActivity.class);
                startActivity(i);
            }
        });

        Button thirdScreen = (Button) findViewById(R.id.button3);
        thirdScreen.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent third = new Intent (MainActivity.this, ThirdActivity.class);
                startActivity(third);
            }
        });
    }
}
